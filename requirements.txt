Django==1.3.1
PIL==1.1.7
South==0.7.3
django-debug-toolbar==0.8.5
django-taggit==0.9.3
sorl-thumbnail==11.09
wsgiref==0.1.2
