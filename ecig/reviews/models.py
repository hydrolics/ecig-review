from django.db import models
from django.contrib.auth.models import User
from manufacturers.models import Manufacturer

from django.forms import ModelForm

from django.db.models.signals import m2m_changed
from django.dispatch import receiver
from django.db.models import Avg

from taggit.managers import TaggableManager
from sorl.thumbnail import ImageField

class Liquid(models.Model):
  name = models.CharField(max_length=255)
  description = models.TextField(blank=True)
  image = ImageField(upload_to='liquid', blank=True)
  manufacturer = models.ForeignKey(Manufacturer, blank=True, null=True)

  comments = models.ManyToManyField('Comment', blank=True)
  reviews = models.ManyToManyField('Review', blank=True)
  avg_flavor = models.DecimalField(max_digits=4, decimal_places=3, blank=True, null=True)
  avg_throat_hit = models.DecimalField(max_digits=4, decimal_places=3, blank=True, null=True)
  avg_vapor = models.DecimalField(max_digits=4, decimal_places=3, blank=True, null=True)
  

  tags = TaggableManager(blank=True)

  created = models.DateTimeField(auto_now_add=True)
  modified = models.DateTimeField(auto_now=True)
  visible = models.BooleanField(default=True)

  def __unicode__(self):
    return self.name
  
  @models.permalink
  def get_absolute_url(self):
    return ('reviews.views.liquid_view', [str(self.id)])


class Comment(models.Model):
  text = models.TextField(max_length=255)
  user = models.ForeignKey(User)

  created = models.DateTimeField(auto_now_add=True)
  modified = models.DateTimeField(auto_now=True)
  visible = models.BooleanField(default=True)

  def __unicode__(self):
    return self.text

class CommentForm(ModelForm):
  class Meta:
    model = Comment
    fields = ('text',)

class Review(models.Model):
  user = models.ForeignKey(User)

  flavor = models.IntegerField(blank=True, null=True)
  throat_hit = models.IntegerField(blank=True, null=True)
  vapor = models.IntegerField(blank=True, null=True)

  created = models.DateTimeField(auto_now_add=True)
  modified = models.DateTimeField(auto_now=True)
  visible = models.BooleanField(default=True)

  def __unicode__(self):
    return unicode(self.user)



@receiver(m2m_changed, sender=Liquid.reviews.through, dispatch_uid='update_avg_identifier')
def update_avgs(sender, instance, action, reverse, model, pk_set, **kwargs):
  ''' Update the parent model, Liquid, with the average ratings of all the reviews '''

  results = Liquid.objects.filter(pk=instance.pk).aggregate(
    Avg('reviews__flavor'),
    Avg('reviews__throat_hit'),
    Avg('reviews__vapor')
  )

  try:
    instance.avg_flavor = results['reviews__flavor__avg']
  except KeyError:
    pass
  
  try:
    instance.avg_throat_hit = results['reviews__throat_hit__avg']
  except KeyError:
    pass

  try:
    instance.avg_vapor = results['reviews__vapor__avg']
  except KeyError:
    pass

  # TODO: Replace average with baysian weighted average  
  instance.save()
