from reviews.models import Liquid, Comment, Review
from django.contrib import admin

admin.site.register(Liquid)
admin.site.register(Comment)
admin.site.register(Review)
