from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('reviews.views',
  url(r'^$', 'index', name='liquidIndex'),
  #url(r'(?P<starts_with>[a-z,A-Z]{1})/$', 'index', name='liquidIndex'),
  url(r'(?P<liquid_id>\d+)/$', 'liquid_view', name='liquidView'),
  url(r'(?P<liquid_id>\d+)/comment/add/$', 'liquid_comment_add', name='liquidCommentAdd'),
  url(r'(?P<liquid_id>\d+)/review/add/$', 'liquid_review_add', name='liquidReviewAdd'),
)
