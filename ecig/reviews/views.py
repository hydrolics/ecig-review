from reviews.models import Liquid, CommentForm, Review

from django.template import RequestContext
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.utils import simplejson
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def index(request, starts_with='a'):
  ''' Returns a list of Liquids that start with a specific letter '''
  liquids = Liquid.objects.filter(name__istartswith=starts_with)
    
  return render_to_response('reviews/index.html', {'liquids': liquids}, context_instance=RequestContext(request))
  
def liquid_view(request, liquid_id):
  liquid = get_object_or_404(Liquid, pk=liquid_id)
  
  if request.user.is_authenticated():
    commentset = CommentForm()
  else:
    commentset = None
  
  return render_to_response(
    'reviews/liquid.html',
    {
      'liquid': liquid,
      'commentset': commentset  
    },
    context_instance=RequestContext(request))
  
def liquid_comment_add(request, liquid_id):

  liquid = get_object_or_404(Liquid, pk=liquid_id)

  if request.method == "POST":
    formset = CommentForm(request.POST)
    print formset
    
    if formset.is_valid():

      liquid.comments.create(
        text = formset.cleaned_data['text'],
        user = request.user
      )

  if request.user.is_authenticated():
    commentset = CommentForm()
  
  return render_to_response(
    'reviews/liquid.html',
    {
      'liquid': liquid,
      'commentset': commentset  
    },
    context_instance=RequestContext(request))
    
    
def liquid_review_add(request, liquid_id):
  if request.method == "POST":
    response = {}

    liquid = Liquid.objects.get(pk=liquid_id)

    review, created = liquid.reviews.get_or_create(user = request.user)
    setattr(review, request.POST['type'], request.POST['rating'])
    review.save()

    return HttpResponse(simplejson.dumps(response), mimetype="application/json")
    
    
def liquid_tag_add(request):
  pass
