from django.db import models
from sorl.thumbnail import ImageField

class Manufacturer(models.Model):
  name = models.CharField(max_length=255)
  description = models.TextField(blank=True)
  image = ImageField(upload_to='manufacturer', blank=True)
  url = models.URLField(blank=True)
  
  created = models.DateTimeField(auto_now_add=True)
  modified = models.DateTimeField(auto_now=True)
  visible = models.BooleanField(default=True)
  
  def __unicode__(self):
    return self.name
