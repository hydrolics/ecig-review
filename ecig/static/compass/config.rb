#discover '/var/lib/gems/1.8/gems/compass-0.11.5/frameworks'
# Require any additional compass plugins here.
# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "../css"
sass_dir = "sass"
images_dir = "../images"
javascripts_dir = "../javascripts"
# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true
